# arnaudgolfouse

PhD student, working on [creusot](https://github.com/creusot-rs/creusot) ; 
Rust enthusiast

Github : https://github.com/arnaudgolfouse

Webpage : https://home.lmf.cnrs.fr/Perso/ArnaudGolfouse
